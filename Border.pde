public class Border {

  public PVector a, b;
  
  public Border(PVector a, PVector b) {
    this.a = a;
    this.b = b;
  }
  
  public void show() {
    line(a.x, a.y, b.x, b.y);
  }
  
}
