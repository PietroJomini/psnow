import java.util.Vector;
import java.util.Iterator;
import gab.opencv.OpenCV;
import processing.video.*;

Capture video;

OpenCV cv;
PImage src;
BordersSet borders;
Snow snow;

Boolean have_photo = false;


void setup() {

  printArray(Capture.list());
  video = new Capture(this);
  video.start();
  
  src = createImage(video.width, video.height, ARGB);
  size(1180, 720);

  cv = new OpenCV(this, src);
  borders = new BordersSet(cv);
  
  snow = new Snow(1000);
  

}

void mouseClicked(){
  
  have_photo = true;
  
}

void draw() {
  
  surface.setSize(src.width, src.height);
  background(255);
  
  if (!have_photo) {
    if (video.available()) {
      video.read();
      src.copy(video, 0, 0, video.width, video.height, 0, 0, src.width, src.height);
    }
  } else {
    snow.letItSnow(borders.getBorders());
  }
  
  
  tint(255, 100);
  image(src, 0, 0);
  
  borders.setSource(src);
  borders.parseBorders(5);

  Iterator border = borders.getBorders().iterator();
  while(border.hasNext()) {
    ( (Border) border.next()).show();
  }
  
  borders.clear();
  
}

//void draw() {

//  background(255);
//  surface.setSize(src.width, src.height);

//  //borders.parseBorders(mouseX*20/width | 1);

//  //tint(255, 50);
//  image(src, 0, 0);
  
//  borders.setSource(src);
//  borders.parseBorders(15);

//  //Iterator border = borders.getBorders().iterator();
//  //while(border.hasNext()) {
//  //  ( (Border) border.next()).show();
//  //}
  
//  snow.letItSnow(borders.getBorders());
//  borders.clear();
  
//  println(frameRate);

//}
